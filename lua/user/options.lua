local options = {
  number = true,
  relativenumber = true,
  shell = "zsh",
  scrolloff = 8,
  splitright = true,
  splitbelow = true,
  updatetime = 300,
  mouse = "a",
  termguicolors = true,
  hlsearch = true,
  smartcase = true,
  ruler = true,
  cmdheight=0,
  expandtab = true,
  shiftwidth = 4,
  tabstop = 4,
  smarttab = true
}
-- for key = 1, options do
  -- vim.opt[key] =
-- end
-- o.clipboard = "unamedplus"
-- o.undofile = true

for key, value in pairs(options) do
  vim.opt[key] = value
end


vim.cmd[[colorscheme tokyonight-night]]


