local key = vim.keymap.set
local opts = { noremap = true, silent = true }

vim.g.mapleader = " "
-- leader i to invert something with nvim-toggler
key("i", "jk", "<ESC>", opts)
key("i", "kj", "<ESC>", opts)
key("n", "<leader>x", ":!xdg-open % &<cr><cr>", opts)
key("n", "<leader>ff", ":Telescope find_files<cr>", opts)
key("n", "<leader>fh", ":Telescope help_tags<cr>", opts)
key("n", "<leader>fg", ":Telescope live_grep<cr>", opts)
key("n", "<leader>fl", ":Telescope luasnip<cr>", opts)
-- splits
key("n", "<C-h>", "<C-w><C-h>", opts)
key("n", "<C-j>", "<C-w><C-j>", opts)
key("n", "<C-k>", "<C-w><C-k>", opts)
key("n", "<C-l>", "<C-w><C-l>", opts)
-- ctrl + w _ makes the current split taller
-- ctrl + w | makes the current split wider
-- ctrl + w = makes the current splits equal
-- ctrl + W T makes the split a tab
-- ctrl + W o closes all windows but the current one
-- TODO get moving text in visual mode to fucking work
-- key("v", "<A-j>", ":m .+1<CR>", opts)

-- formatting
key("n", "<leader>fm", ":lua vim.lsp.buf.format { async = true}<CR>", opts)
