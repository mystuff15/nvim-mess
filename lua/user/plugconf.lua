require("lualine").setup {
  options = {
    theme = "tokyonight"
  }
}
require"nvim-treesitter.configs".setup {
  refactor = {
    -- highlight_definitions = { enable = true, clear_on_cursor_move = true, },
    smart_rename = { enable = true, keymaps = { smart_rename = "grr", },},
    navigation = { enable = true, keymaps = {
      goto_definition = "gnd",
      list_definitions = "gnD",
      list_definitions_toc = "gO",
      goto_next_usage = "<a-*>",
      goto_previous_usage = "<a-#>",
      },
    },
  },
}
require'alpha'.setup(require'alpha.themes.dashboard'.config)
require("guess-indent").setup()
require("nvim-autopairs").setup() -- {}
require("nvim-surround").setup()
require("trouble").setup()
require("indent_blankline").setup{
  space_char_blankline = " ",
  show_current_context = true,
  show_current_context_start = true
}
