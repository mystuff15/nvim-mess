require("null-ls").setup({
	sources = {
		require("null-ls").builtins.formatting.stylua,
		require("null-ls").builtins.diagnostics.luacheck,
		require("null-ls").builtins.completion.luasnip,
		require("null-ls").builtins.diagnostics.djlint,
	},
})
