return require("packer").startup(function(use)
	use "wbthomason/packer.nvim" --packer manages packer
	use "nvim-lualine/lualine.nvim" -- statusline requires devicons and alpha
	use "b3nj5m1n/kommentary" -- comment stuff out
	use "folke/tokyonight.nvim" -- theme
	use "nguyenvukhang/nvim-toggler" -- toggler with leader i
  use "goolord/alpha-nvim" -- dashboard
  use "lewis6991/impatient.nvim" --makes this less slow
  use "nmac427/guess-indent.nvim"
  use "sam4llis/nvim-lua-gf"
  use "kylechui/nvim-surround"
  use "windwp/nvim-autopairs"
  use "lukas-reineke/indent-blankline.nvim"
	--telescope stuff
	use "nvim-telescope/telescope.nvim" -- fuzzy finder
	use "nvim-lua/plenary.nvim" -- telescope dependency
	use "kyazdani42/nvim-web-devicons" -- also used by lualine
	-- tresitter stuff
	use { "nvim-treesitter/nvim-treesitter", run = ":TSUpdate" }
	use "nvim-treesitter/nvim-treesitter-refactor"
	use "windwp/nvim-ts-autotag"--thinking of replacing for cmp
	-- nvim-cmp for autocompletion stuff
	use "L3MON4D3/LuaSnip" -- snippet engine
  use "rafamadriz/friendly-snippets" -- preset up snippets
  use "saadparwaiz1/cmp_luasnip"
	use "hrsh7th/nvim-cmp"
  use "hrsh7th/cmp-path"
  use "hrsh7th/cmp-cmdline"
  use "hrsh7th/cmp-buffer"
  use "hrsh7th/cmp-nvim-lsp"
  -- LSP plugins
  use "williamboman/mason.nvim"
  use "jose-elias-alvarez/null-ls.nvim"
  use "neovim/nvim-lspconfig"
  use "williamboman/mason-lspconfig.nvim"
  use "nvim-lua/lsp-status.nvim"
  use "onsails/lspkind.nvim"
  -- i forgot what this was for
  use "folke/trouble.nvim"
  -- debugging stuff
  use "mfussenegger/nvim-dap"
  use "mfussenegger/nvim-dap-python"
  use "rcarriga/nvim-dap-ui"
end)
